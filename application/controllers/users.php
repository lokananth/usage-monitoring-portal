<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __Construct(){
			parent::__Construct ();
			$this->load->helper(array('form', 'url'));
			$this->load->library('form_validation');
			$this->load->library('session');
			if(!$this->session->userdata('username')) redirect('login');	
			$this->load->helper('api');	
			//error_reporting(0);
	}
	public function index()
	{
		 $this->session->unset_userdata('priCurrentCountrysrc');
		 $this->session->unset_userdata('priCurrentCountrydest');
		 $this->session->unset_userdata('CurrentSourceCountry');
		 $data['pagetitle'] = 'Call Super Market - Users Management';
		 $csm_get_allusers_apiurl = $this->config->item('csm_get_allusers');
		 $data['users_list'] = apiPost($csm_get_allusers_apiurl,$api_postdata='');
		 //echo '<pre>';
		 //print_r($data['users_list']); exit;
		 if($data['users_list'][0]->errcode == -1) {
		 $users_list_data = Array();
		 }
		 else {
		 foreach($data['users_list'] as $users_list)
			 	$users_list_data[] = $users_list;
		 }
		
		 $data['users_list'] = $users_list_data;
		 //echo '<pre>';print_r($data['users_list']); exit;
		 //$data['users_list'] = Array();
		 $this->load->view('header', $data);
		 $this->load->view('inner_header');	
		 $this->load->view('left_menu');
		 $this->load->view('users');
 		 $this->load->view('footer');
	}
	public function action_user()
	{
		//echo '<pre>';
		//print_r($this->input->post()); exit;
		$getUserId = $this->input->post('user_id');
		$getAction = $this->input->post('action');
		if($getAction == 0) {
			$api_postdata['UserId'] = $getUserId;
			//print_r($api_postdata);	exit;
  			$csm_getAPIURL = $this->config->item('csm_deleteuserbyid');
			$user_list = apiPost($csm_getAPIURL, $api_postdata);
			//print_r($user_list); exit;
			if($user_list[0]->errcode == 0)
				echo $user_list[0]->errcode;
			else
				echo $user_list[0]->errmsg;
			//echo $this->input->post('user_id_').'removed';
		}
		else if($getAction == 1) {
			$api_postdata['UserId'] = $getUserId;
			$api_postdata['Status'] = 'Inactive';
  			$csm_getAPIURL = $this->config->item('csm_changeuserstatus');
			$user_list = apiPost($csm_getAPIURL, $api_postdata);
			//print_r($user_list);
			if($user_list[0]->errcode == 0)
				echo $user_list[0]->errcode;
			else
				echo $user_list[0]->errmsg;
			//echo $this->input->post('user_id_').'deactivated';
		}
		else if($getAction == 2) {
			$api_postdata['UserId'] = $getUserId;
			$api_postdata['Status'] = 'Active';
  			$csm_getAPIURL = $this->config->item('csm_changeuserstatus');
			$user_list = apiPost($csm_getAPIURL, $api_postdata);
			//print_r($user_list->errcode);
			if($user_list[0]->errcode == 0)
				echo $user_list[0]->errcode;
			else
				echo $user_list[0]->errmsg;
			
			//echo $this->input->post('user_id_').'reactivated';
		}
		else if($getAction == 3) { //echo $getUserId; exit;
			$api_postdata['UserId'] = $getUserId; 
  			$csm_getAPIURL = $this->config->item('csm_getuserbyid'); 
			$user_list = apiPost($csm_getAPIURL, $api_postdata);
			foreach($user_list as $getusers_list)
			 	$user_list = $getusers_list;
			//echo print_r($user_list); exit;
			//echo $user_list;
			$is_updater = 0;
			$is_approver = 0;
			$is_admin = 0;
			
			$mystring = ''; $findme = ''; $pos = '';
			$mystring = $user_list->UserGroupId;
			$findme   = ',';
			$pos = strpos($mystring, $findme);
			
			if ($pos === false) {
				if($user_list->UserGroupId == 1) 
				$is_updater = 1;
				else if($user_list->UserGroupId == 2)
				$is_approver = 1;
				else if($user_list->UserGroupId == 3)
				$is_admin = 1; 
			} else {
				//if($get_list->UserGroupId != '') { 
				 $UserGroupId = explode(',',$user_list->UserGroupId);
				 if(in_array('1',$UserGroupId))  $is_updater = 1;
				 if(in_array('2',$UserGroupId))  $is_approver = 1 ; 
				 if(in_array('3',$UserGroupId))  $is_admin = 1; 		
				//}
			}
			
			echo $user_list->UserName.'~'.$user_list->Password.'~'.$user_list->FirstName.'~'.$user_list->LastName.'~'.$user_list->Email.'~'.$user_list->Status.'~'.$is_updater.'~'.$is_approver.'~'.$is_admin;
			//echo $user_list->user_name.'~'.$user_list->password.'~'.$user_list->first_name.'~'.$user_list->last_name.'~'.$user_list->Email.'~'.$user_list->Status.'~'.$showChecked;
		}
		else
		return false;
		
	}
	public function user_updation()
	{
		
		//echo '<pre>';
		//print_r($this->input->post()); //exit;
		//echo '0'; exit;
		$getUserId = $this->input->post('edit_user_id');
		$getUsername = trim($this->input->post('user_name'));
		$getPassword = $this->input->post('user_password');
		$getHiddenPassword = $this->input->post('user_hidden_password');
		$getFirstname = trim($this->input->post('first_name'));
		$getLastname = trim($this->input->post('last_name'));
		$getEmail = $this->input->post('user_email');
		$getStatus = $this->input->post('user_status');	
		if($getUserId == '')	
		$getUserrole = $this->input->post('user_new_role');
		else
		$getUserrole = $this->input->post('user_role');
		$getUserrole = implode(',',$getUserrole);
		//print_r($getUserrole); exit;		
		/*if(in_array('1',$getUserrole))  $is_updater = 1; else $is_updater = 0;
		if(in_array('2',$getUserrole)) $is_approver = 1; else $is_approver = 0;
		if(in_array('3',$getUserrole)) $is_admin = 1; else $is_admin = 0;	*/	
		if($getUserId == '') $getUserId = 0; 
		if($getStatus == 1) $getStatus = 'Active'; else $getStatus = 'Inactive';
																
			$api_postdata['UserName'] = $getUsername;
			$api_postdata['Password'] = $getPassword;
			$api_postdata['FirstName'] = $getFirstname;
			$api_postdata['LastName'] = $getLastname;
			$api_postdata['Email'] = $getEmail;
			$api_postdata['Status'] = $getStatus;
			$api_postdata['UserGroup'] = $getUserrole;
			$api_postdata['Created_ModifyBy'] = $this->session->userdata('username');
			$api_postdata['UseId'] = $getUserId;
			//print_r($api_postdata); //exit;
			$csm_getAPIURL = $this->config->item('csm_usercreateupdate');
			$data['user_list'] = apiPost($csm_getAPIURL, $api_postdata);
		//echo '<pre>';
		//print_r($data['user_list']); exit;
		if($data['user_list'][0]->errcode == 0)
		{
			if($getUserId=='') 
				echo 0;
			else
			echo 1;
		}
		else
		echo $data['user_list'][0]->errmsg;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */