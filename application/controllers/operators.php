<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operators extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __Construct(){
			parent::__Construct ();
			$this->load->helper(array('form', 'url'));
			$this->load->library('form_validation');
			$this->load->library('session');
			if(!$this->session->userdata('username')) redirect('login');
			$this->load->helper('api');	
	}
	public function index()
	{
		$data['errmessage'] = '';
		$data['successmessage'] = '';
		$this->session->unset_userdata('priCurrentCountrysrc');
		$this->session->unset_userdata('priCurrentCountrydest');
		if(isset($_FILES['bulk_upload'])){
			require_once APPPATH."/third_party/PHPExcel.php";
			ini_set("display_errors",1);
			
			$config['upload_path'] = './uploaded_sheets/';
			$config['allowed_types'] = 'xls|xlsx|csv';
			$config['max_size']	= '0';
			//echo '<pre>';
			//print_r($this->input->post()); 
			//print_r($_FILES);
			
			//exit;
			
			$this->load->library('upload', $config);
			$field='bulk_upload';
			if ($this->upload->do_upload($field)) {
				$data = array('upload_data' => $this->upload->data());
				$file = $data['upload_data']['full_path'];  
				
				//$file = './upload_sheet/test.xls';
				//load the excel library
				$this->load->library('excel');
				//read file from path
				$objPHPExcel = PHPExcel_IOFactory::load($file);
				//get only the Cell Collection
				$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
				//extract to a PHP readable array format
				foreach ($cell_collection as $cell) {
					$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
					$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
					$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
					//header will/should be in row 1 only. of course this can be modified to suit your need.
					if ($row == 1) {
						$header[$row][$column] = $data_value;
					} else {
						$arr_data[$row][$column] = $data_value;
					}
				}
				//send the data in an array format
				$data['header'] = $header;
				$data['values'] = $arr_data;
				//echo '<pre>';
				//print_r($header);		 exit;
				if(isset($header[1]['A']) && isset($header[1]['B']) && isset($header[1]['C']) && isset($header[1]['D']) && isset($header[1]['E'])) {
					$yes = 0;
					 $getHomeCCode = $this->session->userdata('CurrentSourceCountry');
					 $getCurrArr =  $this->config->item($getHomeCCode); 
					 //$data['Currency'] = $getCurrArr;
					//echo strtolower(trim($header[1]['A']));
					if(strtolower(trim($header[1]['A'])) == 'destination country')
					$yes = 1;
					else
					$yes = 0;
					
					if(strtolower(trim($header[1]['B'])) == 'land line')
					$yes = 1;
					else
					$yes = 0;
	//echo 'land line ('.$getCurrArr.'/Min)'.$yes.strtolower(trim($header[1]['B'])); exit;
					if(strtolower(trim($header[1]['C'])) == 'mobile')
					$yes = 1;
					else
					$yes = 0;
	
					if(strtolower(trim($header[1]['D'])) == 'sms')
					$yes = 1;
					else
					$yes = 0;
	
					if(strtolower(trim($header[1]['E'])) == 'data')
					$yes = 1;
					else
					$yes = 0;
					//echo $yes; exit;
				if($yes == 1) {
					//print_r($arr_data);	 exit;
					//echo count($arr_data);
					$bulk_upload_data = '';
					//print_r($arr_data);
					$total_row = count($arr_data)+2;
					$bulk_upload_data .= '<bulk_load>';
					for($kk=2;$kk<$total_row;$kk++)
					{
						//echo 'hi'; exit;
						if(isset($arr_data[$kk]['A']) && isset($arr_data[$kk]['B']) && isset($arr_data[$kk]['C']) && isset($arr_data[$kk]['D']) && isset($arr_data[$kk]['E'])) { //echo 'hi'; exit;
						
							if(strtolower($arr_data[$kk]['B']) == 'n/a')
							$arr_data[$kk]['B'] = '-111';
							if(strtolower($arr_data[$kk]['C']) == 'n/a')
							$arr_data[$kk]['C'] = '-111';
							if(strtolower($arr_data[$kk]['D']) == 'n/a')
							$arr_data[$kk]['D'] = '-111';
							if(strtolower($arr_data[$kk]['E']) == 'n/a')
							$arr_data[$kk]['E'] = '-111';
					
							$bulk_upload_data .= '<price><country>'.$arr_data[$kk]['A'].'</country><lanline>'.$arr_data[$kk]['B'].'</lanline><mobile>'.$arr_data[$kk]['C'].'</mobile><sms>'.$arr_data[$kk]['D'].'</sms><data>'.$arr_data[$kk]['E'].'</data></price>';
						}
					}
					$bulk_upload_data .= '</bulk_load>';
					
					$api_postdataxml['in_xml_data'] = $bulk_upload_data;
					$api_postdataxml['SourceCountry'] = $this->session->userdata('CurrentSourceCountry');//$this->input->post('bulkSourceCountry');
					$api_postdataxml['Operator_Id'] = $this->input->post('bulkoperatorID');
		
					$pgt_bulkupdate_apiurl = $this->config->item('csm_BulkUploadRates');
					 
					$getxmlResult = apiPost($pgt_bulkupdate_apiurl, $api_postdataxml);
					//echo '<pre>'; print_r($api_postdataxml); print_r($getxmlResult); exit;
					if($getxmlResult[0]->errcode == -1) {
						$data['errmessage'] = $getxmlResult[0]->errmsg;
						$data['successmessage'] = '';
					}
					else
					{	//echo 'hi'; exit;
						if($getxmlResult[0]->errcode == -1)
						$data['errmessage'] = $getxmlResult[0]->errmsg;
						else {	
						$data['successmessage'] = 'Rate has been uploaded successfully!.';
						$data['errmessage'] = '';
						//send email to approver from updater
						$html = 'Hi Team,<br><br> New rates has been uploaded in the CSM Portal.<br><br>Thanks and Regards,<br>'.ucfirst($this->session->userdata('username'));
			
					$this->load->library('email');
					$this->email->clear(TRUE);
					 $config = array (
						  'mailtype' => 'html',
						  'charset'  => 'utf-8',
						  'priority' => '1',
						   );
					$this->email->initialize($config);
					$this->email->from('no-reply@mundio.com');
					//$this->email->to('p.ramachandran@mundio.com'); 
					
					 $csm_get_allusers_apiurl = $this->config->item('csm_get_allusers');
					 $data['users_list'] = apiPost($csm_get_allusers_apiurl,$api_postdata='');
					 //echo '<pre>';
					 //print_r($data['users_list']); exit;
					 if($data['users_list'][0]->errcode == -1) {
					 $users_list_data = Array();
					 }
					 else {
					 foreach($data['users_list'] as $users_list) {
							$is_updater = 0;
							$is_approver = 0;
							$is_admin = 0;
							
							$mystring = ''; $findme = ''; $pos = '';
							$mystring = $users_list->UserGroupId;
							$findme   = ',';
							$pos = strpos($mystring, $findme);
							
							if ($pos === false) {
								if($users_list->UserGroupId == 1) 
								$is_updater = 1;
								else if($users_list->UserGroupId == 2)
								$is_approver = 1;
								else if($users_list->UserGroupId == 3)
								$is_admin = 1; 
							} else {
								//if($get_list->UserGroupId != '') { 
								 $UserGroupId = explode(',',$users_list->UserGroupId);
								 if(in_array('1',$UserGroupId))  $is_updater = 1;
								 if(in_array('2',$UserGroupId))  $is_approver = 1 ; 
								 if(in_array('3',$UserGroupId))  $is_admin = 1; 		
								//}
							}
							if($is_approver == 1) {
							$this->email->to($users_list->Email); 
							//$this->email->to('p.ramachandran@mundio.com'); 
							}
							
					 }
					 }
					
								
					//$html_email = $this->load->view('html_email_order');
					$this->email->subject('CSM Portal - New Rates');
					$this->email->message($html);
					$this->email->send(); 
						
						
						
						}
						
					}
				}
				else {
					$data['errmessage'] = 'Please upload correct format that is header format!.';
					$data['successmessage'] = '';
				}
				}
				else {
					$data['successmessage'] = '';
					$data['errmessage'] = 'Please upload correct format file.';
				}
				
				//exit;
			}
			else
			{
				$error = array('error' => $this->upload->display_errors());
				//print_r($error); exit;
				$data['successmessage'] = '';
				$data['errmessage'] = $error['error'];  
			}
			
			
			
			
		}
		if($this->session->userdata('check_approver') == 1 ) {
			$csm_getOpList_apiurl = $this->config->item('csm_GetApprovalOPList'); 
			$getOP_list = apiPost($csm_getOpList_apiurl,$api_postdataSelect='');
			if($getOP_list[0]->errcode == -1)
			$op_list_data = Array();
			else {
			 foreach($getOP_list as $operators_list)
					$op_list_data[] = $operators_list;
			 }
			
			 $data['operators_list'] = $op_list_data;
			
		}elseif($this->input->post('SourceCountry'))	{ //echo 'hy'; exit;
			
			$getInputSoruceCountry = $this->input->post('SourceCountry');
			$this->session->set_userdata('CurrentSourceCountry',$getInputSoruceCountry);
			$api_postdataSelect['CountryId'] = $getInputSoruceCountry;	
			$csm_getOpList_apiurl = $this->config->item('csm_ListofOperatorbyCountry'); 
			$getOP_list = apiPost($csm_getOpList_apiurl,$api_postdataSelect);
			//print_r($api_postdataSelect);
			if($getOP_list[0]->errcode == -1)
			$op_list_data = Array();
			else {
			 foreach($getOP_list as $operators_list)
					$op_list_data[] = $operators_list;
			 }
			
			 $data['operators_list'] = $op_list_data;
			 $data['selectedCountryId'] = $getInputSoruceCountry;	
		}
		elseif($this->session->userdata('CurrentSourceCountry')) {
			$api_postdataSelect['CountryId'] = $this->session->userdata('CurrentSourceCountry');	
			$csm_getOpList_apiurl = $this->config->item('csm_ListofOperatorbyCountry'); 
			$getOP_list = apiPost($csm_getOpList_apiurl,$api_postdataSelect);
			if($getOP_list[0]->errcode == -1)
			$op_list_data = Array();
			else {
			 foreach($getOP_list as $operators_list)
					$op_list_data[] = $operators_list;
			 }
			
			 $data['operators_list'] = $op_list_data;
			 $data['selectedCountryId'] = $this->session->userdata('CurrentSourceCountry');	
			
		}
		else {		
		  $data['operators_list'] = Array();
		  $data['selectedCountryId'] = '';
		}
		
			$GetOpId = '';
			$api_postdata['OperatorId'] = $GetOpId;
			$api_postdata['flag'] = 3;
			//echo '<pre>'; print_r($api_postdata); exit;
			$csm_unswappedcountry_apiurl = $this->config->item('csm_GetOPCountryList'); 
			$data['unswappedcountry_list'] = apiPost($csm_unswappedcountry_apiurl,$api_postdata);
		
			if($data['unswappedcountry_list'][0]->errcode == -1) 
				$unSwapped_list_data = Array();
			else {
			 foreach($data['unswappedcountry_list'] as $unswappedcountry_list)
					$unswapped_list_data[] = $unswappedcountry_list;
			 }
			
			 $data['unswappedCountry_list'] = $unswapped_list_data;
		
		 $data['pagetitle'] = 'Call Super Market - Operators';
		 $data['vdhome_list'] = Array();
		 $data['vd_list'] = Array();
 		 $getHomeCCode = $this->session->userdata('CurrentSourceCountry');
		 $getCurrArr =  $this->config->item($getHomeCCode); 
		 $data['Currency'] = $getCurrArr;
		 
		 
		 $this->load->view('header', $data);
		 $this->load->view('inner_header');		 
		 $this->load->view('left_menu');
		 $this->load->view('operators',$data);
 		 $this->load->view('footer');
	}

	public function get_operator_details()
	{
		if(!$this->session->userdata('username')) redirect('login');
		$Code = $this->input->post('code');
		$api_postdatavd['in_product_code'] = $this->session->userdata('productcode');
		//$api_postdatavd['in_country_code'] = $Code;		
		$api_postdatavd['in_home_country_code'] = $Code;	
		
		$csm_BrandDetails_apiurl = $this->config->item('csm_gethomeCountrylist'); 
		$getvd_list = apiPost($csm_BrandDetails_apiurl,$api_postdatavd);
		//echo '<pre>'; print_r($api_postdatavd); print_r($getvd_list); exit;
		foreach($getvd_list as $get_list)
			 	$get_list_data = $get_list;
				$vd_list = $get_list_data;
		if($vd_list->errcode == 0) {
			echo $vd_list->country.'~'.$vd_list->country_code.'~'.$vd_list->Landline.'~'.$vd_list->Mobile.'~'.$vd_list->Text.'~'.$vd_list->Data.'~'.$vd_list->status.'~'.$vd_list->Last_updated.'~'.$vd_list->Type.'~'.$vd_list->date;
		}
		else
		echo $vd_list->errmsg;
		
	}
	public function operator_delete() {
		//echo '0'; exit;
		$GetOpId = $this->input->post('deleteopid');
		$GetCountry = $this->input->post('deleteSourceCountry');
		$api_postdata['OperatorId'] = $GetOpId;	
		$csm_approve_apiurl = $this->config->item('csm_DeleteOperator'); 
		$getResult = apiPost($csm_approve_apiurl, $api_postdata);
		//echo '<pre>'; print_r($api_postdata); exit; print_r($getResult); exit;
		if($getResult[0]->errcode == -1)
			$getData = $getResult[0]->errmsg;
		else	
		echo $getResult[0]->errcode;
	}
	public function operator_approve() {
		$GetOpId = $this->input->post('actionopid');
		$GetAction = $this->input->post('getActionStatusAR');
		$AppCountryOperator = $this->input->post('ActionSourceCountry');
		$fromCountry = $this->session->userdata('CurrentSourceCountry');

		if($GetAction == 'approve') $status = 'Y'; else $status = 'N';
		if($this->session->userdata('check_approver') == 1) 
		$api_postdatavd['CountryId'] = $AppCountryOperator;
		else
		$api_postdatavd['CountryId'] = $this->session->userdata('CurrentSourceCountry');
		$api_postdatavd['OperatorId'] = $GetOpId;	
		$api_postdatavd['Status'] = $status;	
		$api_postdatavd['ApprovedBy'] = $this->session->userdata('username');
		$csm_approve_apiurl = $this->config->item('csm_GetOperatorRateaAR'); 
		$getResult = apiPost($csm_approve_apiurl, $api_postdatavd);
		//echo '<pre>'; print_r($api_postdatavd);   print_r($getResult); exit;
		if($getResult[0]->errcode == -1)
			$getData = $getResult[0]->errmsg;
		else {	
		//$status = 'Y'; else $status = 'N'
			if($status == 'Y') echo 0; else echo 1;
		//echo $getResult[0]->errcode;
		}
	}
	
	public function operator_updation() {
		//echo '<pre>'; print_r($this->input->post()); exit;
		$getID = $this->input->post('vd_rate_edit_id'); 
		$GetOpId = $this->input->post('operator_id');
		$GetOperator_Name = $this->input->post('operator_name');
		$GetOperatorCountry = implode(',',$this->input->post('to_select_list'));
		
		if($getID == '') {
			$api_postdata['Operator_Code'] = $GetOpId;
			$api_postdata['Operator_Name'] = $GetOperator_Name;	
			$api_postdata['Op_Countrylist'] = $GetOperatorCountry;			
			$api_postdata['Created_By'] = $this->session->userdata('username');
			$csm_addOp_apiurl = $this->config->item('csm_AddOperatorinfo'); 
			//echo '<pre>'; print_r($api_postdata); exit;
			$getResult = apiPost($csm_addOp_apiurl, $api_postdata);
		} else {
			$api_postdata['Operator_Id'] = $getID;
			$api_postdata['Operator_Code'] = $GetOpId;
			$api_postdata['Operator_Name'] = $GetOperator_Name;	
			$api_postdata['Op_Countrylist'] = $GetOperatorCountry;			
			$api_postdata['Modify_By'] = $this->session->userdata('username');
			$csm_updOp_apiurl = $this->config->item('csm_UpdateOperatorRateId'); 
			//echo '<pre>'; print_r($api_postdata); exit;
			$getResult = apiPost($csm_updOp_apiurl, $api_postdata);
		}
		
		//echo '<pre>'; echo 'getid'.$getID; print_r($api_postdata); print_r($getResult); exit;
		if($getResult[0]->errcode == 0)
		{
			if($getID=='') 
				echo 0;
			else
			echo 1;
		}
		else
		echo $getResult[0]->errmsg;
		
	}

	public function getunmappedCountry()
	{		
			$api_postdataUnswapped = '';
			$tms_unswappedcountry_apiurl = $this->config->item('csm_getRemainingCountry'); 
			$data['unswappedcountry_list'] = apiPost($tms_unswappedcountry_apiurl,$api_postdataUnswapped);
			//echo '<pre>';
			//print_r($data['unswappedcountry_list']); exit;
			if($data['unswappedcountry_list'][0]->errcode == -1) {
				echo $options = ''; exit;
			}
				
			$CountryOptions = $data['unswappedcountry_list'];
		
			$options = '';
			
			if(count($CountryOptions)>=1) {
				for($i=0;$i<count($CountryOptions);$i++)  
				$options .= '<option value="'.$CountryOptions[$i]->Country_Id.'">'.$CountryOptions[$i]->country_Name.'</option>';
			}
			echo $options;
			//}
			//else echo 0;
	}
	public function getunmappedCountryByoperator()
	{		
			$GetOpId = $this->input->post('operatorID');
			$api_postdata['OperatorId'] = $GetOpId;
			$api_postdata['flag'] = 2;
			//echo '<pre>'; print_r($api_postdata); exit;
			$csm_unswappedcountry_apiurl = $this->config->item('csm_GetOPCountryList'); 
			$data['unswappedcountry_list'] = apiPost($csm_unswappedcountry_apiurl,$api_postdata);
			//echo '<pre>';
			//print_r($data['unswappedcountry_list']); exit;
			if($data['unswappedcountry_list'][0]->errcode == -1) {
				echo $options = ''; exit;
			}
				
			$CountryOptions = $data['unswappedcountry_list'];
		
			$options = '';
			
			if(count($CountryOptions)>=1) {
				for($i=0;$i<count($CountryOptions);$i++)  
				$options .= '<option value="'.$CountryOptions[$i]->Country_Id.'">'.$CountryOptions[$i]->country_Name.'</option>';
			}
			echo $options;
			//}
			//else echo 0;
	}
	public function getmappedCountrybyID()
	{		
	
			$GetOpId = $this->input->post('operatorID');
			$api_postdata['OperatorId'] = $GetOpId;
			$api_postdata['flag'] = 1;
			//echo '<pre>'; print_r($api_postdata); exit;
			$csm_unswappedcountry_apiurl = $this->config->item('csm_GetOPCountryList'); 
			$data['unswappedcountry_list'] = apiPost($csm_unswappedcountry_apiurl,$api_postdata);
			//echo '<pre>';
			//print_r($data['unswappedcountry_list']); exit;
			if($data['unswappedcountry_list'][0]->errcode == -1) {
				echo $options = ''; exit;
			}
				
			$CountryOptions = $data['unswappedcountry_list'];
		
			$options = '';
			
			if(count($CountryOptions)>=1) {
				for($i=0;$i<count($CountryOptions);$i++)  
				$options .= '<option value="'.$CountryOptions[$i]->Country_Id.'">'.$CountryOptions[$i]->country_Name.'</option>';
			}
			echo $options; exit;
	
	
			/*$api_postdataUnswapped = '';
			$tms_unswappedcountry_apiurl = $this->config->item('tms_getUnswappedCountry'); 
			$data['unswappedcountry_list'] = apiPost($tms_unswappedcountry_apiurl,$api_postdataUnswapped);
			//echo '<pre>';
			//print_r($data['unswappedcountry_list']); exit;
			if($data['unswappedcountry_list'][0]->errcode == -1) {
				echo $options = ''; exit;
			}
				
			$CountryOptions = $data['unswappedcountry_list'];*/
		
			
			//}
			//else echo 0;
	}
	
	
	public function view_operator_rate() {
		//echo 'hi'; exit;
		 //print_r($this->input->post()); exit;
		 $getHomeCCode = $this->session->userdata('CurrentSourceCountry');
		 $getCurrArr =  $this->config->item($getHomeCCode); 
		 $Currency = $getCurrArr;
		 if($Currency == '')	
		 $Currency = $this->config->item('allC');
		 /*$accRes = '';
		  $accRes .= '<table id="view_operator_rate_table_new" class="table table-striped vd_rates_table table-bordered display test responsive nowrap" width="100%">       <thead>
                    <tr>
                      <th data-class="expand">Destination Country</th>
                      <th data-hide="phone,phoneL,tabletP">LL('.$Currency.'/Min)</th>
                      <th data-hide="phone,phoneL,tabletP">Mob('.$Currency.'/Min)</th>
                      <th data-hide="phone,phoneL,tabletP">SMS('.$Currency.'/Text)</th>
                      <th data-hide="phone,phoneL,tabletP">Data('.$Currency.'/MB)</th>
                    </tr>
                  </thead>
                  <tbody>';
		 $accRes .= '<tr><td>123</td><td>123</td><td>123</td><td>123</td><td>123</td></tr>';
			$accRes .= '</tbody></table>';
			echo $accRes; exit;*/
		if($this->session->userdata('check_approver') == 1) 
		$countryId = $this->input->post('ViewSourceCountry');
		else
		$countryId = $this->session->userdata('CurrentSourceCountry');
		$api_postdata['CountryId'] = $countryId;
		$api_postdata['OperatorId'] = $this->input->post('ViewoperatorID');
		$csm_opRate_apiurl = $this->config->item('csm_GetOperatorRateInd'); 
		$getResult = apiPost($csm_opRate_apiurl,$api_postdata);
		//echo '<pre>'; print_r($getResult); exit;
		
		if($getResult[0]->errcode == -1) 
			$getResult = Array();
		$this->session->set_userdata('ViewSourceCountryDownload',$countryId);
		$this->session->set_userdata('ViewOperatorIDDownload',$api_postdata['OperatorId']);
		
		 $accRes = '';
		 $accRes .= '<table id="view_operator_rate_table_new" class="table table-striped vd_rates_table table-bordered display test responsive nowrap" width="100%">       <thead>
                    <tr>
                     <th data-class="expand">Destination Country</th>
                      <th data-hide="phone,phoneL,tabletP">LL('.$Currency.'/Min)</th>
                      <th data-hide="phone,phoneL,tabletP">Mob('.$Currency.'/Min)</th>
                      <th data-hide="phone,phoneL,tabletP">SMS('.$Currency.'/Text)</th>
                      <th data-hide="phone,phoneL,tabletP">Data('.$Currency.'/MB)</th>
                    </tr>
                  </thead>
                  <tbody>';
				  //print_r($getResult);
			foreach($getResult as $getList)	{   
			if($getList->lanline == '-111') $getList->lanline = 'N/A';
			if($getList->Mobile == '-111') $getList->Mobile = 'N/A';
			if($getList->SMS == '-111') $getList->SMS = 'N/A';
			if($getList->Data == '-111') $getList->Data = 'N/A';
			$accRes .= '<tr><td>'.$getList->country.'</td><td>'.$getList->lanline.'</td><td>'.$getList->Mobile.'</td><td>'.$getList->SMS.'</td><td>'.$getList->Data.'</td></tr>';
			}
			$accRes .= '</tbody></table>';
			echo $accRes;
		 exit;
		 
		 $this->load->view('operator_rate_view'); exit;
	}
	public function download_rate_updater() {
		 
		
		$api_postdataSelect['CountryId'] = $this->session->userdata('CurrentSourceCountry');	
		$csm_getOpList_apiurl = $this->config->item('csm_ListofOperatorbyCountry'); 
		$getOP_list = apiPost($csm_getOpList_apiurl,$api_postdataSelect);
		if($getOP_list[0]->errcode == -1)
		$op_list_data = Array();
		else {
		 foreach($getOP_list as $operators_list)
				$op_list_data[] = $operators_list;
		 }
		
		 $reports = $op_list_data;
		 $getHomeCCode = $this->session->userdata('CurrentSourceCountry');
		 $Currency =  $this->config->item($getHomeCCode); 		
	
		
		
		date_default_timezone_set('Europe/London');
		$this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Rates Report');

        for ($col = 'A'; $col != 'J'; $col++) {
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:D1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Rates Report on '.date('d-m-Y'));
        $this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        
        $this->excel->getActiveSheet()->getStyle('A2:D2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle('A2:D2')->getFill()->getStartColor()->setARGB('58C849');
        $this->excel->getActiveSheet()->getStyle("A2:D2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        $this->excel->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2:D2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->excel->getActiveSheet()->getStyle('A2:D2')->getFont()->getColor()->setRGB('FFFFFF');;
		$this->excel->getActiveSheet()->getStyle('A3:D3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
		$this->excel->getActiveSheet()->getStyle('A3:A256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('B3:B256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('C3:C256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('D3:D256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);		
						
        $this->excel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'Operator ID')
                ->setCellValue('B2', 'Operator Name')
                ->setCellValue('C2', 'Last Uploaded Date')
				->setCellValue('D2', 'Status');
				

        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
        

        $i = 3;

		
        
        foreach ($reports AS $item) {
            $this->excel->getActiveSheet()->getStyle('A' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('B' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('C' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->excel->getActiveSheet()->getStyle('D' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

                    
			if($item->Approval_Status=='Y')  $status = 'Approved'; elseif($item->Approval_Status=='N')  $status = 'Rejected'; else  $status = 'Not Approved'; 
					
            // Miscellaneous glyphs, UTF-8
            $this->excel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $item->Op_Code)
                    ->setCellValue('B' . $i, $item->Op_Name)
                    ->setCellValue('C' . $i, $item->LastUploaded_Date)
					->setCellValue('D' . $i, $status );
            $i++;
        }

        $filename = ' Rates Report - '.date('d-m-Y').'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
		ob_end_clean();
		ob_start();
        $objWriter->save('php://output');
		exit;
		
    
	}
	public function download_rate_approverAdmin() {
		 
		
		//$api_postdataSelect['CountryId'] = $this->session->userdata('CurrentSourceCountry');	
		$csm_getOpList_apiurl = $this->config->item('csm_GetApprovalOPList'); 
		$getOP_list = apiPost($csm_getOpList_apiurl,$api_postdataSelect='');
		if($getOP_list[0]->errcode == -1)
		$op_list_data = Array();
		else {
		 foreach($getOP_list as $operators_list)
				$op_list_data[] = $operators_list;
		 }
		
		 $reports = $op_list_data;
		 //echo '<pre>';
		 //print_r($reports); exit;
		 $getHomeCCode = $this->session->userdata('CurrentSourceCountry');
		 $Currency =  $this->config->item($getHomeCCode); 		
	
		
		
		date_default_timezone_set('Europe/London');
		$this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Rates Report');

        for ($col = 'A'; $col != 'J'; $col++) {
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:D1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Rates Report on '.date('d-m-Y'));
        $this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFill()->getStartColor()->setARGB('58C849');
        $this->excel->getActiveSheet()->getStyle("A2:E2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        $this->excel->getActiveSheet()->getStyle("A2:E2")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFont()->getColor()->setRGB('FFFFFF');;
		$this->excel->getActiveSheet()->getStyle('A3:E3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
		$this->excel->getActiveSheet()->getStyle('A3:A256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('B3:B256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('C3:C256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('D3:D256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);		
						
        $this->excel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'Operator ID')
                ->setCellValue('B2', 'Operator Name')
                ->setCellValue('C2', 'Source Country')
				->setCellValue('D2', 'Last Uploaded Date')
				->setCellValue('E2', 'Status');
				

        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
        

        $i = 3;

		
        
        foreach ($reports AS $item) {
            $this->excel->getActiveSheet()->getStyle('A' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('B' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('C' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->excel->getActiveSheet()->getStyle('D' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->excel->getActiveSheet()->getStyle('E' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                  
			if($item->Approve_Status=='Y')  $status = 'Approved'; elseif($item->Approve_Status=='N')  $status = 'Rejected'; else  $status = 'Not Approved'; 
					
            // Miscellaneous glyphs, UTF-8
            $this->excel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $item->Op_Code)
                    ->setCellValue('B' . $i, $item->Op_Name)
                    ->setCellValue('C' . $i, $item->country_Name)
					->setCellValue('D' . $i, $item->LastUpdated_Date)
					->setCellValue('E' . $i, $status );
            $i++;
        }

        $filename = ' Rates Report - '.date('d-m-Y').'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
		ob_end_clean();
		ob_start();
        $objWriter->save('php://output');
		exit;
		
    
	}
	
	public function download_view_operator_rateInd() {

		$countryId = $this->session->userdata('ViewSourceCountryDownload');
		$api_postdata['CountryId'] = $countryId;
		$api_postdata['OperatorId'] = $this->session->userdata('ViewOperatorIDDownload');
		$csm_opRate_apiurl = $this->config->item('csm_GetOperatorRateInd'); 
		$getResult = apiPost($csm_opRate_apiurl,$api_postdata);
				
	
		if($getResult[0]->errcode == -1)
		$op_list_data = Array();
		else {
		 foreach($getResult as $operators_list)
				$op_list_data[] = $operators_list;
		 }
		
		 $reports = $op_list_data; //print_r($api_postdata);
		 //print_r($reports); exit;
		 $getHomeCCode = $this->session->userdata('CurrentSourceCountry');
		 $Currency =  $this->config->item($getHomeCCode); 		
		 if($Currency == '')	
		 $Currency = $this->config->item('allC');
		
		
		date_default_timezone_set('Europe/London');
		$this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Rates Report');

        for ($col = 'A'; $col != 'J'; $col++) {
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:E1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Rates Report on '.date('d-m-Y'));
        $this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFill()->getStartColor()->setARGB('58C849');
        $this->excel->getActiveSheet()->getStyle("A2:E2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        $this->excel->getActiveSheet()->getStyle("A2:E2")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFont()->getColor()->setRGB('FFFFFF');;
		$this->excel->getActiveSheet()->getStyle('A3:E3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
		$this->excel->getActiveSheet()->getStyle('A3:A256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('B3:B256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('C3:C256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('D3:D256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);		
		$this->excel->getActiveSheet()->getStyle('E3:E256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						
        $this->excel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'Destination Country')
                ->setCellValue('B2', 'Land Line ('.$Currency.'/Min)')
                ->setCellValue('C2', 'Mobile ('.$Currency.'/Min)')
				->setCellValue('D2', 'SMS ('.$Currency.'/Text)')
				->setCellValue('E2', 'Data ('.$Currency.'/MB)');
				

        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
        

        $i = 3;

		
        
        foreach ($reports AS $item) {
            $this->excel->getActiveSheet()->getStyle('A' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('B' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('C' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->excel->getActiveSheet()->getStyle('D' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->excel->getActiveSheet()->getStyle('E' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);			
                    
					if($item->lanline == '-111') $item->lanline = 'N/A';
					if($item->Mobile == '-111') $item->Mobile = 'N/A';
					if($item->SMS == '-111') $item->SMS = 'N/A';
					if($item->Data == '-111') $item->Data = 'N/A';
					
            // Miscellaneous glyphs, UTF-8
            $this->excel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $item->country)
                    ->setCellValue('B' . $i, $item->lanline)
                    ->setCellValue('C' . $i, $item->Mobile)
					->setCellValue('D' . $i, $item->SMS)
					->setCellValue('E' . $i, $item->Data);
            $i++;
		}


        $filename = ' Rates Report - '.date('d-m-Y').'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
		ob_end_clean();
		ob_start();
        $objWriter->save('php://output');
		exit;
		
    
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */