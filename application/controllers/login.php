<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __Construct(){
			parent::__Construct ();
			$this->load->helper(array('form', 'url'));
			$this->load->library('form_validation');
			$this->load->library('session');
			$this->load->helper('api');	
	}
	public function index()
	{
		
		if($this->input->post()) {
				$getUsername = $this->input->post('username');
				$this->session->set_userdata('username',$getUsername);
				redirect('operators');				
				$getUserPassword = $this->input->post('password');
				$api_postdata['UserName'] = $getUsername;
				$api_postdata['Password'] = $getUserPassword; 
				$csm_login_apiurl = $this->config->item('csm_login_api');
				$getResult = apiPost($csm_login_apiurl, $api_postdata);
				/*echo '<pre>';
				print_r($api_postdata);
				print_r($getResult); exit;	*/
				if(empty($getResult)) {
					$this->session->set_flashdata('err_csm_login','Something went wrong!.Please try again.');
					redirect('login');	
				}
				elseif($getResult[0]->errcode == 0) { //echo count($getResult[0]->errcode); exit;
					$this->session->set_userdata('username',$getUsername);
					
					$is_updater = 0;
					$is_approver = 0;
					$is_admin = 0;
					
					$mystring = ''; $findme = ''; $pos = '';
					$mystring = $getResult[0]->UserGroupId;
					$findme   = ',';
					$pos = strpos($mystring, $findme);
					
					if ($pos === false) {
						if($getResult[0]->UserGroupId == 1) 
						$is_updater = 1;
						else if($getResult[0]->UserGroupId == 2)
						$is_approver = 1;
						else if($getResult[0]->UserGroupId == 3)
						$is_admin = 1; 
					} else {
						//if($get_list->UserGroupId != '') { 
						 $UserGroupId = explode(',',$getResult[0]->UserGroupId);
						 if(in_array('1',$UserGroupId))  $is_updater = 1;
						 if(in_array('2',$UserGroupId))  $is_approver = 1 ; 
						 if(in_array('3',$UserGroupId))  $is_admin = 1; 		
						//}
					}
					//echo $is_updater;
					//echo $is_approver;
					//echo $is_admin; exit;
					
					$this->session->set_userdata('check_updater',$is_updater);
					$this->session->set_userdata('check_approver',$is_approver);
					$this->session->set_userdata('check_admin',$is_admin);
					redirect('operators');
				}
				else {
					$this->session->set_flashdata('err_csm_login',$getResult[0]->errmsg);
					redirect('login');	
				}
		}
		if($this->session->userdata('username')) { //echo 'dfd'; exit;
				//	redirect('operators');
		}
			
		 $data['pagetitle'] = 'Call Super Market - Login';
		 $this->load->view('header', $data);
		 $this->load->view('login');
 		 $this->load->view('footer');
	}
	public function change_password()
	{
		$api_postdata['UserName'] = $this->session->userdata('username');
		$api_postdata['NewPassword'] = $this->input->post('new_password');
		$api_postdata['OldPassword'] = $this->input->post('old_password');
		$api_postdata['Modifiedby'] = $this->session->userdata('username');
				//print_r($api_postdata); exit;
		$csm_change_password_apiurl = $this->config->item('csm_change_password'); 
		$get_result = apiPost($csm_change_password_apiurl, $api_postdata);
		//print_r($get_result);	 exit;
		if($get_result[0]->errcode == -1)
			echo $get_result[0]->errmsg;
		else
			echo $get_result[0]->errcode;
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */