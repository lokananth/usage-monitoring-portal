<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Priority extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __Construct(){
			parent::__Construct ();
			$this->load->helper(array('form', 'url'));
			$this->load->library('form_validation');
			$this->load->library('session');
			if(!$this->session->userdata('username')) redirect('login');
			$this->load->helper('api');	
	}
	public function index()
	{
		$this->session->unset_userdata('CurrentSourceCountry');
		$data['pagetitle'] = 'Call Super Market - Change Priority';
		$data['vdhome_list'] = Array();
		$data['vd_list'] = Array();
		$data['Currency'] = 'p';
		
		if($this->input->post('pri_source_country') && $this->input->post('pri_dest_country') )	{ //echo 'hy'; exit;
			$getInputSoruceCountry = $this->input->post('pri_source_country');
			$getInputdestCountry = $this->input->post('pri_dest_country');
			$this->session->set_userdata('priCurrentCountrydest',$getInputdestCountry);
			$this->session->set_userdata('priCurrentCountrysrc',$getInputSoruceCountry);
			$api_postdataSelect['Source_CountryId'] = $getInputSoruceCountry;	
			$api_postdataSelect['Destination_CountryId'] = $getInputdestCountry;				
			$csm_getOpList_apiurl = $this->config->item('csm_GetPriorityList'); 
			$getOP_list = apiPost($csm_getOpList_apiurl,$api_postdataSelect);
			//echo '<pre>'; print_r($api_postdataSelect); print_r($getOP_list); exit;
			if($getOP_list[0]->errcode == -1)
			$op_list_data = Array();
			else {
			 foreach($getOP_list as $operators_list)
					$op_list_data[] = $operators_list;
			 }
			
			 $data['operators_list'] = $op_list_data;
			 $data['selectedSourceCountryId'] = $getInputSoruceCountry;	
 			 $data['selectedDestCountryId'] = $getInputdestCountry;	
			 
		}
		elseif($this->session->userdata('priCurrentCountrydest'))	{ //echo 'hy'; exit;
			$getInputSoruceCountry = $this->session->userdata('priCurrentCountrysrc');
			$getInputdestCountry =$this->session->userdata('priCurrentCountrydest');
			$api_postdataSelect['Source_CountryId'] = $getInputSoruceCountry;	
			$api_postdataSelect['Destination_CountryId'] = $getInputdestCountry;				
			$csm_getOpList_apiurl = $this->config->item('csm_GetPriorityList'); 
			$getOP_list = apiPost($csm_getOpList_apiurl,$api_postdataSelect);
			//echo '<pre>'; print_r($api_postdataSelect); print_r($getOP_list); exit;
			if($getOP_list[0]->errcode == -1)
			$op_list_data = Array();
			else {
			 foreach($getOP_list as $operators_list)
					$op_list_data[] = $operators_list;
			 }
			
			 $data['operators_list'] = $op_list_data;
			 $data['selectedSourceCountryId'] = $getInputSoruceCountry;	
 			 $data['selectedDestCountryId'] = $getInputdestCountry;	
		}
		else {
			$data['selectedSourceCountryId'] = '';	
			$data['selectedDestCountryId'] = '';
			$data['operators_list'] =  Array();	
			
		}
		
		$api_postdataUnswapped = '';
		$tms_unswappedcountry_apiurl = $this->config->item('csm_getRemainingCountry'); 
		$data['unswappedcountry_list'] = apiPost($tms_unswappedcountry_apiurl,$api_postdataUnswapped);
		//echo '<pre>';
		//print_r($data['unswappedcountry_list']); exit;
		if($data['unswappedcountry_list'][0]->errcode == -1) 
			$unSwapped_list_data = Array();
		else {
		 foreach($data['unswappedcountry_list'] as $unswappedcountry_list)
				$unswapped_list_data[] = $unswappedcountry_list;
		 }
		 
		$GetOpId = '';
		$api_postdatasource['OperatorId'] = $GetOpId;
		$api_postdatasource['flag'] = 3;
		//echo '<pre>'; print_r($api_postdata); exit;
		$csm_unswappedcountry_apiurlsource = $this->config->item('csm_GetOPCountryList'); 
		$data['unswappedCountry_listSource'] = apiPost($csm_unswappedcountry_apiurlsource,$api_postdatasource);
	
		if($data['unswappedCountry_listSource'][0]->errcode == -1) 
			$unswapped_list_datas = Array();
		else {
		 foreach($data['unswappedCountry_listSource'] as $unswappedcountry_list)
				$unswapped_list_datas[] = $unswappedcountry_list;
		 }
		
		 $data['unswappedCountry_listSource'] = $unswapped_list_datas;
		 
		
		 $data['unswappedCountry_list'] = $unswapped_list_data;
		 //$data['operators_list'] =  Array();
	     $getHomeCCode = $this->session->userdata('priCurrentCountrysrc');
		 $getCurrArr =  $this->config->item($getHomeCCode); 
		 if($getCurrArr == '')
		 $getCurrArr = $this->config->item('allC');
		 $data['Currency'] = $getCurrArr;

		 $this->load->view('header', $data);
		 $this->load->view('inner_header');		 
		 $this->load->view('left_menu');
		 $this->load->view('priority',$data);
 		 $this->load->view('footer');
	}

	public function piority_update() {
		//echo '0'; exit;
		$api_postdata['Id'] = $this->input->post('operatorID');	
		$api_postdata['Priority'] = $this->input->post('priority_no');	
		
		$csm_updatePriority_apiurl = $this->config->item('csm_UpdatePriority'); 
		$getResult = apiPost($csm_updatePriority_apiurl, $api_postdata);
		//echo '<pre>'; print_r($api_postdata); print_r($getResult); exit;
		if($getResult[0]->errcode == -1)
			$getData = $getResult[0]->errmsg;
		else	
		echo $getResult[0]->errcode;
	}
	
	public function download_priority_list() {

		$getInputSoruceCountry = $this->session->userdata('priCurrentCountrysrc');
		$getInputdestCountry =$this->session->userdata('priCurrentCountrydest');
		$api_postdataSelect['Source_CountryId'] = $getInputSoruceCountry;	
		$api_postdataSelect['Destination_CountryId'] = $getInputdestCountry;				
		$csm_getOpList_apiurl = $this->config->item('csm_GetPriorityList'); 
		$getOP_list = apiPost($csm_getOpList_apiurl,$api_postdataSelect);
		//echo '<pre>'; print_r($api_postdataSelect); print_r($getOP_list); exit;
		if($getOP_list[0]->errcode == -1)
		$op_list_data = Array();
		else {
		 foreach($getOP_list as $operators_list)
				$op_list_data[] = $operators_list;
		 }
			
		
		 $reports = $op_list_data; //print_r($api_postdata);
		 //print_r($reports); exit;
		 $getHomeCCode = $this->session->userdata('priCurrentCountrysrc');
		 $Currency =  $this->config->item($getHomeCCode); 
		
		
		date_default_timezone_set('Europe/London');
		$this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Priority Report');

        for ($col = 'A'; $col != 'J'; $col++) {
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:E1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Priority Report on '.date('d-m-Y'));
        $this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        
        $this->excel->getActiveSheet()->getStyle('A2:G2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle('A2:G2')->getFill()->getStartColor()->setARGB('58C849');
        $this->excel->getActiveSheet()->getStyle("A2:G2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        $this->excel->getActiveSheet()->getStyle("A2:G2")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2:G2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->excel->getActiveSheet()->getStyle('A2:G2')->getFont()->getColor()->setRGB('FFFFFF');;
		$this->excel->getActiveSheet()->getStyle('A3:G3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
		$this->excel->getActiveSheet()->getStyle('A3:A256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('B3:B256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('C3:C256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('D3:D256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);		
		$this->excel->getActiveSheet()->getStyle('E3:E256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						
        $this->excel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'Operator ID')
                ->setCellValue('B2', 'Operator Name')
                ->setCellValue('C2', 'Land Line ('.$Currency.'/Min)')
                ->setCellValue('D2', 'Mobile ('.$Currency.'/Min)')
				->setCellValue('E2', 'SMS ('.$Currency.'/Text)')
				->setCellValue('F2', 'Data ('.$Currency.'/MB)')
				->setCellValue('G2', 'Priority');
				

        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
        

        $i = 3;

		
        
        foreach ($reports AS $item) {
            $this->excel->getActiveSheet()->getStyle('A' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('B' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('C' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->excel->getActiveSheet()->getStyle('D' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->excel->getActiveSheet()->getStyle('E' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);			
			$this->excel->getActiveSheet()->getStyle('F' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);							            $this->excel->getActiveSheet()->getStyle('G' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);						
            // Miscellaneous glyphs, UTF-8
            $this->excel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $item->Operator_Code)
                    ->setCellValue('B' . $i, $item->Operator_Name)
                    ->setCellValue('C' . $i, $item->LanLine)
					->setCellValue('D' . $i, $item->Mobile)
					->setCellValue('E' . $i, $item->SMS)
					->setCellValue('F' . $i, $item->Data)
					->setCellValue('G' . $i, $item->Priority);										
            $i++;
		}


        $filename = ' Priority Report - '.date('d-m-Y').'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
		ob_end_clean();
		ob_start();
        $objWriter->save('php://output');
		exit;
		
    
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */