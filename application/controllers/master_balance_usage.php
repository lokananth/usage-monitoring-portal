<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_balance_usage extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __Construct(){
			parent::__Construct ();
			$this->load->helper(array('form', 'url'));
			$this->load->library('form_validation');
			$this->load->library('session');
			if(!$this->session->userdata('username')) redirect('login');
			$this->load->helper('api');	
	}
	public function index()
	{
		$data['errmessage'] = '';
		$data['successmessage'] = '';
		$this->session->unset_userdata('priCurrentCountrysrc');
		$this->session->unset_userdata('priCurrentCountrydest');

		
		 $data['pagetitle'] = $this->config->item('project_name').' - Bundle Usage';
 		 $getHomeCCode = $this->session->userdata('CurrentSourceCountry');
		 $getCurrArr =  $this->config->item($getHomeCCode); 
		 $data['Currency'] = $getCurrArr;
		 
		 
		 $this->load->view('header', $data);
		 $this->load->view('inner_header');		 
		 $this->load->view('left_menu');
		 $this->load->view('master_balance_usage');
 		 $this->load->view('footer');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */