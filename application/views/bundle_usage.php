<div id="main" role="main">
			<!-- page header -->
          <div class="pageheader">           

            <div class="breadcrumbs">
              <ol class="breadcrumb">
                <li>You are here</li>
                <li><a href="index.html">Minimal</a></li>
                <li><a href="tables.html">Tables</a></li>
                <li class="active">Bootstrap Tables</li>
              </ol>
            </div>


          </div>
          <!-- /page header -->
		<!-- MAIN CONTENT -->
			<div id="content">

				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h3 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-home"></i> Tariff Testing </h3><!-- <span>&nbsp;>&nbsp; Device List</span> -->
					</div> 
				</div>
				<!-- widget grid -->
				<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
				
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
							<!-- Widget ID (each widget will need unique ID)-->
							
							<!-- end widget -->
				
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blueLight jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
								<header>
									<span class="widget-icon"> <i class="fa fa-minus-circle"></i> </span> 
									<h2>Master Balance </h2>
									<div class="jarviswidget-ctrls" role="menu">   <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus"></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a></div>	
				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
										<div class="col-sm-12 col-md-12 prod-form">
											<div class="row">
												<form class="form-horizontal" action="">
													<div class="col-sm-12 col-md-6 col-md-offset-3">
														<div class="form-group">
															<label class="col-sm-4  line-height32 col-md-3 text-right">Number &nbsp;<span class="pull-right line-height32">:</span></label>
															<div class="col-sm-5">
																<div class="input-group">
																	<input type="text" name="mydate" placeholder="Select a date" class="form-control datepicker hasDatepicker" data-dateformat="dd/mm/yy" id="dp1457600006255">
																	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																</div>
															</div>
															<div class="col-sm-2">
																<button type="submit" class="btn btn-info">GO</button>
															</div>
														</div>														
													</div>													
												</form>
											</div>
											
										</div>										
									</div>
								</div>
							</div>
							
							
							<div class="jarviswidget jarviswidget-color-blueLight jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
								<header>
									<span class="widget-icon"> <i class="fa fa-minus-circle"></i> </span>
									<h2>Bundles</h2>
									<div class="jarviswidget-ctrls" role="menu">   <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus"></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a></div>	
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">			
										<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
												<tr>
													<th data-class="expand">CLI</th>
													<th data-hide="phone">Brand</th>
													<th data-hide="phone,tablet">Customer Name</th>
													<th data-hide="phone">Calls</th>
													<th data-hide="phone">SMS</th>
													<th data-hide="phone">Data / (MB)</th>
													<th data-hide="phone,tablet">Prev. History</th>
													
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><a href="#" data-toggle="modal" data-target="#myModal1">4465656665</a></td>
													<td>VMUK</td>
													<td>Siva</td>
													<td>200</td>
													<td>15</td>
													<td>150</td>
													<td>10</td>
												</tr>
												
												<tr>
													<td><a href="#" data-toggle="modal" data-target="#myModal1">4465656665</a></td>
													<td>VMUK</td>
													<td>Siva</td>
													<td>200</td>
													<td>15</td>
													<td>150</td>
													<td>10</td>
												</tr>
												
												<tr>
													<td><a href="#" data-toggle="modal" data-target="#myModal1">4465656665</a></td>
													<td>VMUK</td>
													<td>Siva</td>
													<td>200</td>
													<td>15</td>
													<td>150</td>
													<td>10</td>
												</tr>

												<tr>
													<td><a href="#" data-toggle="modal" data-target="#myModal1">4465656665</a></td>
													<td>VMUK</td>
													<td>Siva</td>
													<td>200</td>
													<td>15</td>
													<td>150</td>
													<td>10</td>
												</tr>
												<tr>
													<td><a href="#" data-toggle="modal" data-target="#myModal1">4465656665</a></td>
													<td>VMUK</td>
													<td>Siva</td>
													<td>200</td>
													<td>15</td>
													<td>150</td>
													<td>10</td>
												</tr>
												<tr>
													<td><a href="#" data-toggle="modal" data-target="#myModal1">4465656665</a></td>
													<td>VMUK</td>
													<td>Siva</td>
													<td>200</td>
													<td>15</td>
													<td>150</td>
													<td>10</td>
												</tr>
												<tr>
													<td><a href="#" data-toggle="modal" data-target="#myModal1">4465656665</a></td>
													<td>VMUK</td>
													<td>Siva</td>
													<td>200</td>
													<td>15</td>
													<td>150</td>
													<td>10</td>
												</tr>
												<tr>
													<td><a href="#" data-toggle="modal" data-target="#myModal1">4465656665</a></td>
													<td>VMUK</td>
													<td>Siva</td>
													<td>200</td>
													<td>15</td>
													<td>150</td>
													<td>10</td>
												</tr>
												<tr>
													<td><a href="#" data-toggle="modal" data-target="#myModal1">4465656665</a></td>
													<td>VMUK</td>
													<td>Siva</td>
													<td>200</td>
													<td>15</td>
													<td>150</td>
													<td>10</td>
												</tr>												
											</tbody>
										</table>
									
									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
							
				<!-- modal-->
				 <div class="modal fade" id="myModal1" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Add balance</h4>
							</div>
							<div class="modal-body">								
								<div class="row">
									<form class="form-horizontal" >
										<div class="form-group">
											<label class="col-sm-4 control-label">Current Balance</label>
											<div class="col-sm-6">
												<p class="form-control-static">200 GBP</p>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Add Amount</label>
											<div class="col-sm-6 col-xs-9">
												<input type="text" class="form-control" />
											</div>
										</div>			
										<div class="form-group m-t-20">
											<label class="col-sm-4 control-label">Comments</label>
											<div class="col-sm-6 col-xs-9">
												<input type="text" class="form-control" />
											</div>
										</div>
									</form>
								</div>
								<div class="row m-t-20">
									<div class="jarviswidget jarviswidget-color-blueLight jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
										<header>
											<h2>Log Reference</h2>
						
										</header>				
										<!-- widget div-->
										<div>					
											<!-- widget edit box -->									
											<!-- end widget edit box -->					
											<!-- widget content -->
												<div class="widget-body no-padding">	
												
												<table id="datatable_tablemodal" class="table table-striped table-bordered table-hover" width="100%">
													<thead>
														<tr>
															<th data-class="expand">Issue ID</th>
															<th data-hide="phone,tablet">Raised Date</th>
															<th data-hide="phone,tablet">Clarification</th>
															<th data-hide="phone,tablet">Clarification</th>
															<th data-hide="phone,tablet">Clarification</th>
															
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1023</td>
															<td>1023</td>
															<td>1023</td>
															<td>1023</td>
															<td>device failure</td>
														</tr>
														<tr>
															<td>1023</td>
															<td>1023</td>
															<td>1023</td>
															<td>1023</td>
															<td>device failure</td>
														</tr>
														<tr>
															<td>1023</td>
															<td>1023</td>
															<td>1023</td>
															<td>1023</td>
															<td>device failure</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>									
								</div>									
							</div>
							<div class="modal-footer text-center">
								<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true" >
									Allow
								</button>
								<button type="button" class="btn btn-info"  >
									Block
								</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->		
	
				
	<!-- modal-->
				 <div class="modal fade" id="myModal3" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Threshold Limit Updated</h4>
							</div>
							<div class="modal-body dsp-block" >
								<div class="col-sm-12">
									<div class="row">			
										<strong class="text-center">New Threshold Limit has been updated successfully</strong>
									</div>
								</div>
							</div>
							<div class="modal-footer text-center">
								<button type="button" class="btn btn-primary" data-dismiss="modal">
									Close
								</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
	<!-- modal end-->	
	
						</article>
						<!-- WIDGET END -->
				
					</div>
				
					<!-- end row -->
				
					<!-- end row -->
				
				</section>
				<!-- end widget grid -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>