<ul class="nav nav-tabs">
	<li <?php if($this->uri->segment(1) == 'operators') { ?> class="active" <?php } ?>><a href="<?php echo site_url('operators'); ?>">Manage Operators</a></li>
	<li <?php if($this->uri->segment(1) == 'priority') { ?> class="active" <?php } ?>><a href="<?php echo site_url('priority'); ?>">Change Priority</a></li>
</ul>