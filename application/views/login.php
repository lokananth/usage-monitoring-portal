<div id="main" role="main">
	<!-- MAIN CONTENT -->
			<div id="content">

				<!-- row -->
				<div class="row"> 
					<div class="col-sm-offset-3 col-sm-6 col-xs-12">
					 <div id="content" class=" full-page login">
				  
							<form action="" id="login_form" name="login_form" class="smart-form client-form" method="post">
							 <img src="<?php echo base_url(); ?>img/logo-mundio1.png" alt class="logo"> 
								<header>
									<b>Usage Monitoring Portal</b>
								</header>
								<fieldset> 
									<section>
										<label class="label">Username</label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input type="text" name="username" id="username">
											<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter username</b></label>
									</section>
									<section>
										<label class="label">Password</label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" name="password" id="password">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
										<!-- <div class="note">
											<a href="forgotpassword.html">Forgot password?</a>
										</div> -->
									</section> 
								</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary">
										Sign in
									</button>
								</footer>
							</form>

						</div>
							 
				
						</div>
				
					</div>

				<!-- end row -->

			</div>
			
			</div>