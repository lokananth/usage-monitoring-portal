<div id="main" role="main"> 
    <div id="ribbon">
		<!-- breadcrumb -->
		<ol class="breadcrumb"><li>Home</li><li>Manage Operators</li></ol>
		<!-- end breadcrumb -->

	</div>
  <!-- MAIN CONTENT -->
  <div id="content">

    
    <!-- widget grid -->
    <section id="widget-grid" class="">
    
    <!-- row -->
    <div class="row"> 
      
      <!-- NEW WIDGET START -->
      <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      
      
      <!-- Widget ID (each widget will need unique ID)-->
      <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-togglebutton="false" data-widget-fullscreenbutton="false">
      
								<header>
									<span class="widget-icon"> <i class="fa fa-table"></i> </span>
									<h2>Users </h2>				
								</header> 
      <!-- widget div-->
	  
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <button id="new_user" type="button" class="new_user btn btn-primary txt-color-white m-t-20 m-b-20"><i class="fa fa-plus"></i>&nbsp; New User</button>
      </div>
      <div> 
        
        <!-- widget edit box -->
        <div class="jarviswidget-editbox"> 
          <!-- This area used as dropdown edit box --> 
          
        </div>
        <!-- end widget edit box --> 
        
        <!-- widget content -->
        <div class="widget-body no-padding">
          <table id="datatable_tabletools" class="table table-striped table-bordered" width="100%">
            <thead>
              <tr>
                <th data-class="expand">User Name</th>
                <th>Full Name</th>
                <th data-hide="phone">User Role</th>
                <th data-hide="phone,tablet">Status</th>
                <?php // if($this->session->userdata('check_admin') == 1) {  ?>
                <th data-hide="phone,tablet">Action</th>
                <?php //} ?>
              </tr>
            </thead>
            <tbody>
              <?php //echo '<pre>'; print_r($users_list); exit;
				$i=0;
				foreach($users_list as $get_list) { ?>
              <tr>
                <td><?php echo $get_list->UserName; ?></td>
                <td><?php echo $get_list->FirstName.' '.$get_list->LastName; ?></td>
                <?php  $getRole = ''; $UserGroupId = array(); //$get_list->UserGroupId = '1,2'; 
				$mystring = ''; $findme = ''; $pos = '';
				$mystring = $get_list->UserGroupId;
				$findme   = ',';
				$pos = strpos($mystring, $findme);
				
				if ($pos === false) {
					if($get_list->UserGroupId == 1) 
					$getRole = 'Updater, ';
					else if($get_list->UserGroupId == 2)
					$getRole = 'Approver, '; 
					else if($get_list->UserGroupId == 3)
					$getRole = 'User Admin';
				} else {
					//if($get_list->UserGroupId != '') { 
					 $UserGroupId = explode(',',$get_list->UserGroupId);
					 if(in_array('1',$UserGroupId))  $getRole .= 'Updater, ';
					 if(in_array('2',$UserGroupId))  $getRole .= 'Approver, '; 
					 if(in_array('3',$UserGroupId))  $getRole .= 'User Admin'; 		
					//}
				}
				
				?>
                <td><?php echo rtrim($getRole, ', '); ?></td>
                <td><?php echo $get_list->Status; ?></td>
                 <?php //if($this->session->userdata('check_admin') == 1) {  ?>
                <td> <?php if($get_list->UserId != 1) { ?><button class="btn btn-danger edit_user" id="edit_user_<?php echo $get_list->UserId; ?>" name="edit_user_<?php echo $get_list->UserId; ?>" ><i class="fa fa-pencil"></i></button>
                  <button class="btn btn-danger click_remove_user" id="remove_user_<?php echo $get_list->UserId; ?>" name="remove_user_<?php echo $get_list->UserId; ?>"><i class="fa fa-trash-o"></i></button>
                  <?php if($get_list->Status == 'Active') { ?>
                  <button class="btn btn-danger click_deactivate_user" id="deactivate_user_<?php echo $get_list->UserId; ?>" name="deactivate_user_<?php echo $get_list->UserId; ?>" >Deactivate</button>
                  <?php } else { ?>
                  <button class="btn btn-danger click_activate_user" id="activate_user_<?php echo $get_list->UserId; ?>" name="activate_user_<?php echo $get_list->UserId; ?>" >Activate</button>
                  <?php }} ?></td>
                  <?php //} ?>
              </tr>
              <?php $i++; } ?>
            </tbody>
          </table>
        </div>
        <!-- end widget content --> 
        
      </div>
      <!-- end widget div --> 
      
    </div>
    <!-- end widget --> 
    
    <!-- widget edit box --> 
    
    <!-- end widget edit box -->
    <div class="modal fade" id="PopupUserCreateSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
            <h4 class="modal-title" id="SuccessConfirmation">Success</h4>
          </div>
          <div class="modal-body">
            <p>User has been <span id="update_success"></span> successfully !!!</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal" onclick="reloadPage();" > OK </button>
          </div>
        </div>
      </div>
      <!-- /.modal-content --> 
    </div>
    <div class="modal fade" id="PopupUserActionSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
            <h4 class="modal-title" id="SuccessConfirmation">Success</h4>
          </div>
          <div class="modal-body">
            <p>User has been <span id="ShowActionSuccess"></span> successfully !!!</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal" onclick="reloadPage()" > OK </button>
          </div>
        </div>
      </div>
      <!-- /.modal-content --> 
    </div>
    <div class="modal fade" id="PopupUserAction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
            <h4 class="modal-title" id="SuccessConfirmation">Confirmation</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure want to <span id="getActionText"></span>?</p>
          </div>
          <div class="modal-footer">
            <form action="" name="UserformAction" id="UserformAction" method="post">
              <input type="hidden" name="user_id" id="user_id"  class="form-control" />
              <input type="hidden" name="action" id="action"  class="form-control" />
            </form>
            <button type="button" class="btn btn-success user_form_action_submit" id="user_form_action_submit" name="user_form_action_submit" > OK </button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" > Cancel </button>
          </div>
        </div>
      </div>
      <!-- /.modal-content --> 
    </div>
    <div class="modal fade" id="PopupUserForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
            <h4 class="modal-title" id="myModalLabel"><span id="action_user"></span> User Form</h4>
          </div>
          <form action="" name="Userform" id="Userform" method="post" class="form-horizontal">
            <div class="modal-body">
              <input type="hidden" name="edit_user_id" id="edit_user_id"  class="form-control" />
              <div class="row">
				<div class="col-sm-12">
					<div class="form-group">
					  <div class="col-md-6">
						<label for="first_name">First Name</label>
						<input type="text" name="first_name" id="first_name" class="form-control" onkeydown="lettersonly(this.id)" placeholder="Enter First Name" autocomplete="off" />
					  </div>
					  <div class="col-md-6">
						<label for="last_name">Last Name</label>
						<input type="text" name="last_name" id="last_name"  class="form-control" onkeydown="lettersonly(this.id)" placeholder="Enter Last Name" autocomplete="off" />
					  </div>
					</div>
					<div class="form-group">
					  <div class="col-md-6">
						<label for="user_name">Username</label>
						<input type="text" name="user_name" id="user_name" class="form-control" onkeydown="alphanumeric_only(this.id)" placeholder="Enter User Name" autocomplete="off" />
					  </div>
					  <div class="col-md-6">
						<label for="password">Password</label>
						<input type="password" name="user_password" id="user_password" class="form-control" placeholder="Enter Password"  />
						<input type="hidden" name="user_hidden_password" id="user_hidden_password" class="form-control" placeholder="Enter Password" autocomplete="off"  />
					  </div>
					</div>
					<div class="form-group">
					  <div class="col-md-6">
						<label for="quantity">Email</label>
						<input type="text" class="form-control" id="user_email" name="user_email" placeholder="Enter Email" autocomplete="off" />
					  </div>
					  <div class="col-md-6">
						<label for="voucher_type">Status</label>
						<select class="form-control" id="user_status" name="user_status">
						  <!--<option value="">Select</option>
													<option value="1" selected="selected">Active</option>
													<option value="0">Inactive</option>-->
						</select>
					  </div>
					</div>
				</div>
                <div class="col-md-12">
                  <div class="form-group clearfix">
                    <label class="col-sm-12" for="user_group">User Role</label>
                    <div class="col-md-12">
                    <div id="replace_role">
                      <input type="checkbox" id="user_role_updater" name="user_role[]" value="1" />
                      Updater
                      <input type="checkbox" id="user_role_approver" name="user_role[]" value="2" />
                      Approver
                      <input type="checkbox" id="user_role_user_admin" name="user_role[]" value="3" />
                      User Admin </div>
                      <div id="new_user_role">
                      <input type="checkbox" id="user_newrole_updater" name="user_new_role[]" value="1" />
                      Updater
                      <input type="checkbox" id="user_newrole_approver" name="user_new_role[]" value="2" />
                      Approver
                      <input type="checkbox" id="user_newrole_user_admin" name="user_new_role[]" value="3" />
                      User Admin </div>
                       </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success user_form_submit" id="user_form_submit" name="user_form_submit" > Save </button>
              <button type="button" class="btn btn-danger" data-dismiss="modal"> cancel </button>
            </div>
          </form>
        </div>
      </div>
      <!-- /.modal-content --> 
    </div>
    
    <!-- end widget -->
    
    </article>
    <!-- WIDGET END --> 
    
  </div>
  
  <!-- end row --> 
  
  <!-- end row -->
  
  </section>
  <!-- end widget grid --> 
  
</div>
<!-- END MAIN CONTENT -->

</div>
