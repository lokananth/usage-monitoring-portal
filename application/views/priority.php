<div id="main" role="main">

		<!-- MAIN CONTENT -->
			<div id="ribbon">
				<!-- breadcrumb -->
				<ol class="breadcrumb"><li>Home</li><li>Change Priority</li></ol>
				<!-- end breadcrumb -->

			</div>
			<div id="content">
				<div class="row">
				<div class="col-sm-12 col-xs-12">
					<div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable">
						<header role="heading"><h2>Change Priority</h2></header>
						<div class="col-sm-12 col-xs-12">
							<form class="form-horizontal" name="priority_select_country_form" id="priority_select_country_form" method="post">
								<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12 m-t-20 m-b-20">
									<div class="form-group">
										<label class="col-sm-4">Source Country:</label>
										<div class="col-sm-6 col-xs-12">
											<select class="form-control" name="pri_source_country" id="pri_source_country" >
												<option value="">Select</option>
										<?php foreach($unswappedCountry_listSource as $getCList) { ?>
                                          <option <?php if($selectedSourceCountryId == $getCList->Country_Id) { ?> selected="selected" <?php } ?> value="<?php echo $getCList->Country_Id ?>"><?php echo $getCList->country_Name ?></option>
                                          <?php } ?>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4">Destination Country:</label>
										<div class="col-sm-6 col-xs-12">
											<select class="form-control" name="pri_dest_country" id="pri_dest_country" >
										<option value="">Select</option>
										<?php foreach($unswappedCountry_list as $getCList) { ?>
                                          <option <?php if($selectedDestCountryId == $getCList->Country_Id) { ?> selected="selected" <?php } ?> value="<?php echo $getCList->Country_Id ?>"><?php echo $getCList->country_Name ?></option>
                                          <?php } ?>
											</select>
										</div>
									
                                    <div class="col-sm-2 col-xs-12">
											<button type="submit" id="go" class="btn btn-primary">GO</button>
										</div>
                                    </div>
								</div>
							</form>
						</div>
			

				<!-- widget grid -->
				<section id="widget-grid">
				
					<!-- row -->
					<div class="row">
				
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
							<!-- Widget ID (each widget will need unique ID)-->
							

				
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
								
								<!-- <header>
									<span class="widget-icon"> <i class="fa fa-table"></i> </span>
									<h2>Column Filters </h2>				
								</header> -->
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
                                    
										<table id="datatable_tabletools" class="table table-striped vd_rates_table table-bordered display test responsive nowrap" width="100%" >
                                          <thead>
                                            <tr>
                                              <th data-class="expand">Operator ID</th>
                                              <th data-class="expand">Operator Name</th>
                                              <th data-hide="phone,phoneL,tabletP">Land Line (<?php echo $Currency; ?>/Min)</th>
                                              <th data-hide="phone,phoneL,tabletP">Mobile (<?php echo $Currency; ?>/Min)</th>
                                              <th data-hide="phone,phoneL,tabletP">SMS (<?php echo $Currency; ?>/Text)</th>
                                              <th data-hide="phone,tablet">Data (<?php echo $Currency; ?>/MB)</th>
                                              <th data-hide="">Priority</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <?php  //echo '<pre>'; print_r($operators_list); exit; 
											//echo '<pre>'; print_r($this->session->all_userdata());exit; 
											
                                                foreach($operators_list as $get_list) {  ?>
                                            <tr>
                                              <td><?php echo $get_list->Operator_Code; ?></td>
                                              <td><?php echo $get_list->Operator_Name; ?></td>
                                              <td><?php echo $get_list->LanLine; ?></td>
                                              <td><?php echo $get_list->Mobile; ?></td>
                                              <td><?php echo $get_list->SMS; ?></td>
                                              <td><?php echo $get_list->Data; ?></td>
                                              <?php /*?><td><?php echo $get_list->Priority; ?></td><?php */?>
                                              <td>
                                              <?php if($get_list->Priority == 1 ) { ?>
                                              <select name="update_priority" class="form-control">
                                              <option value="1" selected="selected">1</option>
                                              </select>
                                              <?php } else { ?>
                                              <select name="update_priority" class="form-control update_priority" id="update_priority_<?php echo $get_list->Id.'_'.$get_list->Priority; ?>" >
                                              <option value="">Select</option>
                                              <?php for($i=2;$i<=10;$i++) { ?>
                                              <option value="" <?php if($get_list->Priority == $i ) { ?> selected="selected" <?php } ?>><?php echo $i; ?></option>
                                              <?php } ?>
                                              </select>
                                              <?php } ?>
                                              </td>
                                            </tr>
                                            <?php } ?>
                                          </tbody>
                                        </table>

									</div>
							
								</div>
							</div>   
							
								<div class="col-xs-12 col-sm-12 m-b-20 pd-0">
								<button type="submit" onclick="location.href='priority/download_priority_list'" class="vd_new_rate btn btn-primary"><i class="fa-fw fa fa-download fa-lg"></i> Download</button>
							</div>						
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
											
						</article>
				</div>
						
				
				</section>
				
							<!-- end widget -->
						<!-- WIDGET END -->
				
	
					</div>
				</div>
	
					<!-- end row -->
				</div>
			</div>  
				
					<!-- end row -->
				<!-- end widget grid -->
                                    
                    <div class="modal fade" id="PopupVDRateUpdationSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
                    <div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="SuccessConfirmation">Success</h4>
							</div>
                            <div class="modal-body">
						<p>Rate has been updated successfully !!!</p>
                        </div>
                        <div class="modal-footer">
								<button type="button" class="btn btn-success" data-dismiss="modal" onclick="reloadPage()" >
									OK
								</button>
                                </div>
                    </div>
					
					</div><!-- /.modal-content -->
					</div>
                    
					<div class="modal fade" id="ShowVDEditRateForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="ShowVDpopupTitle">Country Saver Rates</h4>
							</div>
                            <form action="" name="vd_edit_rate_form" class="form-horizontal" id="vd_edit_rate_form" method="post">
                            <input type="hidden" name="vd_rate_edit_id" id="vd_rate_edit_id" />
							<div class="modal-body">
                            	<div class="row">								
											<div class="col-sm-12">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-sm-12 col-xs-12" for="landline">Landline</label>
															<div class="col-sm-8 col-xs-8">
																<input name="vd_landline" id="vd_landline" class="form-control spinner decimal" value="" min="0">
															</div>
															<div class="col-sm-4 col-xs-4 line-height32 pd-0"><?php echo $Currency; ?>/Min</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-sm-12 col-xs-12" for="mobile">Mobile</label>
															<div class="col-sm-8 col-xs-8">
																<input name="vd_mobile" id="vd_mobile" class="form-control spinner decimal" value=""  min="0">
															</div>
															<div class="col-sm-4 col-xs-4 line-height32 pd-0"><?php echo $Currency; ?>/Min</div>
														</div>
													</div>
												</div>
											</div>											
											<div class="col-sm-12">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-sm-12 col-xs-12" for="text">Text (sms)</label>
															<div class="col-sm-8 col-xs-8">
																<input class="form-control spinner decimal" value="" min="0" name="vd_text"  id="vd_text" >
															</div>
															<div class="col-sm-4 col-xs-4 line-height32 pd-0"><?php echo $Currency; ?>/Text</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-sm-12 col-xs-12" for="data">Data</label>
															<div class="col-sm-8 col-xs-8">
																<input class="form-control spinner decimal" value="" min="0"  id="vd_data" name="vd_data" >
															</div>
															<div class="col-sm-4 col-xs-4 line-height32 pd-0"><?php echo $Currency; ?>/MB</div>
														</div>
													</div>
												</div>
											</div>
											
											<div class="col-sm-12">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-sm-12 col-xs-12 radio radio-btn radio-inline" for="radio"><input type="radio" value="immediate" name="effect_type" id="immediate_rate" checked="checked" > Immediate</label>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-sm-12 col-xs-12  radio-btn radio-inline" for="radio"><input type="radio" value="schedule" name="effect_type" id="schedule_rate" > Schedule</label>
														</div>
													</div>
												</div>
											</div>
                                            <div id="show_effect_date">
											<div class="col-sm-12">
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-sm-12 col-xs-12" for="vd_rate_from_date">From Date</label>
															<div class="col-sm-8 col-xs-8">
																<input type="text" name="vd_rate_from_date" readonly="readonly" class="form-control"  id="vd_rate_from_date" />
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-sm-12 col-xs-12" for="vd_rate_to_date">To Date</label>
															<div class="col-sm-8 col-xs-8">
																<input type="text" name="vd_rate_to_date" readonly="readonly" class="form-control"  id="vd_rate_to_date" />
															</div>
														</div>
													</div>
												</div>
											</div>
                                            </div>
                                            
                                </div>
                        	</div>
							<div class="modal-footer">
                                <button type="button" class="btn btn-success vd_rate_submit" id="vd_rate_submit" name="vd_rate_submit" data-dismiss="modal" >
									Save
								</button>
								<button type="button" class="btn btn-danger" data-dismiss="modal">
									Cancel
								</button>
                                </div>
                                </form>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				
</div>
			<!-- END MAIN CONTENT -->