<style>
.hyperlink{
	cursor: pointer;
	
}
</style>
<div id="main" role="main"> 
  
  <!-- MAIN CONTENT -->
  <div id="ribbon"> 
    <!-- breadcrumb -->
    <ol class="breadcrumb">
      <li>Home</li>
      <li>Manage Operators</li>
    </ol>
    <!-- end breadcrumb --> 
    
  </div>
  <div id="content">
    <div class="row">
      <div class="col-sm-12 col-xs-12">
        <div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable">
          <header role="heading">
            <h2>Manage Operators </h2>
          </header>
          <div class="col-sm-12 col-xs-12">
            <form class="form-horizontal" action="" method="post" name="selecCountryForm" id="selecCountryForm">
              <div class="col-md-3 col-sm-4 col-xs-12 m-t-20">
              
                <!--<h4 class="Dtright">Sort By:</h4>-->
              </div>
              <div class="col-md-6 col-sm-8 col-xs-12 m-t-20 m-b-20">
              <?php if($errmessage) { ?>
                            <div class="col-sm-12">
                            <div id="keysuccessmsg" class="alert alert-danger">
                            <p><?php echo $errmessage; ?></p>
                            </div>
                            </div>
                            <?php } ?>
						 <?php if($successmessage) { ?>
                            <div class="col-sm-12">
                            <div id="keysuccessmsg" class="alert alert-success">
                            <p><?php echo $successmessage; ?></p>
                            </div>
                            </div>
                            <?php } ?>
                <!--<div class="form-group">
                  <label class="col-sm-4">Source:</label>
                  <div class="col-sm-6 col-xs-12">
                    <select class="form-control" >
                      <option value="">Country</option>
                      <option value="">Country 2</option>
                      <option value="">Country 3</option>
                    </select>
                  </div>
                </div>-->
                 <?php if($this->session->userdata('check_admin') == 1 || $this->session->userdata('check_updater') == 1) {  ?>
                <div class="form-group">
                  <label class="col-sm-2">Source Country:</label>
                  <div class="col-sm-6 col-xs-12">
                    <select class="form-control" name="SourceCountry" id="SourceCountry" >
                    <option value="">Select</option>
                    <?php foreach($unswappedCountry_list as $getCList) { ?>
                      <option <?php if($selectedCountryId == $getCList->Country_Id) { ?> selected="selected" <?php } ?> value="<?php echo $getCList->Country_Id ?>"><?php echo $getCList->country_Name ?></option>
					<?php } ?>
                    </select>
                  </div>
                  <div class="col-sm-2 col-xs-12">
                    <button type="submit" id="go" class="btn btn-primary">GO</button>
                  </div>
                </div>
                <?php } ?>
              </div>
            </form>
             <?php if($this->session->userdata('check_admin') == 1 || $this->session->userdata('check_approver') == 1) {  ?>
            <div class="col-xs-12 col-sm-12 m-t-20 m-b-20 pd-0">
              <button type="button" class="vd_new_rate btn btn-primary" id="vd_new_rate" ><i class="fa-fw fa fa-plus"></i> Add New Operator</button>
            </div>
            <?php } ?>
          </div>
          
          <!-- widget grid -->
          <section id="widget-grid">
          
          <!-- row -->
          <div class="row"> 
            
            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <!-- Widget ID (each widget will need unique ID)--> 
            
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
            
            <!-- <header>
									<span class="widget-icon"> <i class="fa fa-table"></i> </span>
									<h2>Column Filters </h2>				
								</header> --> 
            
            <!-- widget div-->
            <div> 
              
              <!-- widget edit box -->
              <div class="jarviswidget-editbox"> 
                <!-- This area used as dropdown edit box --> 
                
              </div>
              <!-- end widget edit box --> 
              
              <!-- widget content -->
              <div class="widget-body no-padding">
                <table id="<?php if($this->session->userdata('check_approver') == 1) {  ?>datatable_tabletools_approver<?php } else { ?>datatable_tabletools<?php } ?>" class="table table-striped vd_rates_table table-bordered display test responsive nowrap" width="100%" >
                  <thead>
                    <tr>
                      <th data-class="expand">Operator ID</th>
                      <th data-hide="phone,phoneL,tabletP">Operator Name</th>
                      <?php if($this->session->userdata('check_approver') == 1) {  ?>
                      <th data-hide="phone,phoneL,tabletP">Source Country</th>
                      <?php } ?>
                      <th data-hide="phone,phoneL,tabletP">Last Uploaded Date</th>
                      <th data-hide="phone,phoneL,tabletP">Status</th>
                      <th data-hide="">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php  //echo '<pre>'; print_r($operators_list); exit; 
											//echo '<pre>'; print_r($this->session->all_userdata());exit; 
											
                           foreach($operators_list as $get_list) {  ?>
                    <tr>
                      <td  <?php if($this->session->userdata('check_admin') == 1 || $this->session->userdata('check_approver') == 1) {  ?>class="vd_edit_rate hyperlink" id="vd_edit_rate_<?php echo trim($get_list->Op_id).'_'.$get_list->Op_Name.'_'.$get_list->Op_Code; ?>" <?php } ?>><?php echo $get_list->Op_Code; ?></td>
                      <td><?php echo $get_list->Op_Name; ?></td>
                       <?php if($this->session->userdata('check_approver') == 1) {  ?>
                      <td><?php echo $get_list->country_Name; ?></td>
                       <?php } ?>
                      <td><?php if($this->session->userdata('check_updater') == 1 || $this->session->userdata('check_admin') == 1) echo $get_list->LastUploaded_Date; else echo $get_list->LastUpdated_Date;  ?></td>
                      <td><?php if($this->session->userdata('check_updater') == 1 || $this->session->userdata('check_admin') == 1) { if($get_list->Approval_Status=='Y')  echo 'Approved'; elseif($get_list->Approval_Status=='N') echo 'Rejected'; else echo 'Not Approved'; } else { if($get_list->Approve_Status=='Y')  echo 'Approved'; elseif($get_list->Approve_Status=='N')  echo 'Rejected'; else echo 'Not Approved'; }?></td>
                      <td><?php  if($this->session->userdata('check_admin') == 1) {   ?>
                        <button type="button" class="view_operator_rate btn btn-primary" name="change_status" id="view_operator_rate_<?php echo trim($get_list->Op_id).'_'.$get_list->Op_Name; ?>" value="Update" >View Rates</button>
                        <button type="button" class="showPopupBulkupload btn btn-primary" name="change_status" id="vd_upload_rate_<?php echo trim($get_list->Op_id).'_'.$get_list->Op_Name; ?>" value="upload_rates" >Upload Rates</button>
                        <?php  //if($get_list->status != 'Approved') {   ?>
                        <button type="button" class="view_operator_rate btn btn-primary" name="change_status" id="view_operator_rate_<?php echo trim($get_list->Op_id).'_'.$get_list->Op_Name; ?>" value="Approve" >Approve/Reject Rates</button>
                         <button type="button" class="vd_delete_rate btn btn-primary" name="change_statusdel" id="vd_delete_rate_<?php echo trim($get_list->Op_id).'_'.$get_list->Op_Name; ?>" value="Delete" >Delete Operator</button>
                        <?php //} ?>
                        <?php }  elseif($this->session->userdata('check_updater') == 1) {  ?>
                        <button type="button" class="view_operator_rate btn btn-primary" name="change_status" id="view_operator_rate_<?php echo trim($get_list->Op_id).'_'.$get_list->Op_Name; ?>" value="Update" >View Rates</button>
                         <button type="button" class="showPopupBulkupload btn btn-primary" name="change_status" id="vd_upload_rate_<?php echo trim($get_list->Op_id).'_'.$get_list->Op_Name; ?>" value="upload_rates" >Upload Rates</button>
                        <?php }  elseif($this->session->userdata('check_approver') == 1) {  ?>
                        <?php  if($get_list->Approve_Status=='') {   ?>
                        <button type="button" class="view_operator_rate btn btn-primary" name="change_status" id="view_operator_rate_<?php echo trim($get_list->Op_id).'_'.$get_list->Op_Name.'_'.$get_list->Country_Id; ?>" value="Approve" >Approve/Reject Rates</button>
                        <?php } ?>
                        <button type="button" class="vd_delete_rate btn btn-primary" name="change_statusdel" id="vd_delete_rate_<?php echo trim($get_list->Op_id).'_'.$get_list->Op_Name; ?>" value="Delete" >Delete Operator</button>
                        <?php } //} ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- widget edit box --> 
          
          <!-- end widget edit box -->
          <div class="col-xs-12 col-sm-12 m-b-20 pd-0">
          <?php if($this->session->userdata('check_approver') == 1 || $this->session->userdata('check_admin') == 1) $myhref = 'download_rate_approverAdmin'; else $myhref = 'download_rate_updater';  ?>
            <button type="button" class="vd_new_rate_down btn btn-primary" onclick="location.href='operators/<?php echo $myhref; ?>'"><i class="fa-fw fa fa-download fa-lg"></i> Download</button>
            <!--<button type="button" class="showPopupBulkupload btn btn-primary"><i class="fa-fw fa fa-download fa-lg"></i> Bulk Upload</button>
            <button type="button" class="view_operator_rate btn btn-primary"><i class="fa-fw fa fa-download fa-lg"></i> View Rate</button>-->
          </div>
          </article>
        </div>
        </section>
        
        <!-- end widget --> 
        <!-- WIDGET END --> 
        
      </div>
      
      <!-- end row --> 
    </div>
  </div>
</div>
<div class="modal fade" id="PopupViewOperatorRate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
        <h4 class="modal-title" id="ShowViewORpopupTitle"></h4>
      </div>
      <form action="" name="viewOperatorRateForm" class="form-horizontal" id="viewOperatorRateForm" method="post" enctype="multipart/form-data">
        <input type="hidden" name="ViewSourceCountry" id="ViewSourceCountry" />
        <input type="hidden" name="ViewoperatorID" id="ViewoperatorID" />
        <div class="modal-body">
          <div class="row">
            <div id="view_update_op_rate_table"></div>
          </div>
        </div>
        <div class="modal-footer">
          <p class="col-xs-12 col-sm-4 text-left pd-0"> <button type="button" onclick="location.href='operators/download_view_operator_rateInd'" class="view_op_rate_download btn btn-primary"><i class="fa-fw fa fa-download fa-lg"></i> Download</button> </p>
          <?php  if($this->session->userdata('check_admin') == 1 || $this->session->userdata('check_approver') == 1) {   ?>
          <button type="button" class="btn btn-success vd_approve_rate" id="vd_approve_rate" name="bulk_upload_submit" data-dismiss="modal" > Approve </button>
          <button type="button" class="btn btn-success vd_reject_rate" id="vd_reject_rate" name="bulk_upload_submit" data-dismiss="modal" > Reject </button>
		<?php } ?>
          <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancel </button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content --> 
</div>
<div class="modal fade" id="PopupBulkuploadForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
        <h4 class="modal-title" id="ShowBUpopupTitle"></h4>
      </div>
      <form action="" name="bulk_upload_form" class="form-horizontal" id="bulk_upload_form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="bulkSourceCountry" id="bulkSourceCountry" />
        <input type="hidden" name="bulkoperatorID" id="bulkoperatorID" />
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-4 col-xs-12" for="select_file">Select File</label>
                    <div class="col-sm-8 col-xs-12">
                      <input type="file" name="bulk_upload" id="bulk_upload" value=""  />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <p class="float:left col-xs-12 col-sm-8 text-left pd-0"><strong>Note :</strong>Please upload .<strong>xlsx</strong> file format. For Ex : <a href="./uploaded_sheets/bulk_upload.xlsx" download>Download</a><br />
          System updates currency based on source country. </p>
          <button type="button" class="btn btn-success bulk_upload_submit" id="bulk_upload_submit" name="bulk_upload_submit" data-dismiss="modal" > Save </button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancel </button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content --> 
</div>
<div class="modal fade" id="PopupDeleteOperator" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
        <h4 class="modal-title" id="SuccessConfirmation">Confirmation</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure want to delete this operator?</p>
      </div>
      <div class="modal-footer">
        <form action="" name="operatorDeleteForm" id="operatorDeleteForm" method="post">
          <input type="hidden" name="deleteopid" id="deleteopid"  class="form-control" />
          <input type="hidden" name="deleteSourceCountry" id="deleteSourceCountry"  class="form-control" />
        </form>
        <button type="button" class="btn btn-success operator_delete_submit" id="operator_delete_submit" name="operator_delete_submit" > OK </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" > Cancel </button>
      </div>
    </div>
  </div>
  <!-- /.modal-content --> 
</div>
<div class="modal fade" id="ShowOperatorDeleteSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
        <h4 class="modal-title" id="SuccessConfirmation">Success</h4>
      </div>

      <div class="modal-body">
        <p>Operator has been deleted successfully !!!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="reloadPage()" > OK </button>
      </div>
    </div>
  </div>
  <!-- /.modal-content --> 
</div>
<!-- end row --> 
<!-- end widget grid -->

<div class="modal fade" id="PopupVDRateUpdationSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
        <h4 class="modal-title" id="SuccessConfirmation">Success</h4>
      </div>
      <div class="modal-body">
        <p>Operator has been <span id="op_addor_updateTxt"></span> successfully !!!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="reloadPage()" > OK </button>
      </div>
    </div>
  </div>
  <!-- /.modal-content --> 
</div>
<div class="modal fade" id="ShowVDEditRateForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
        <h4 class="modal-title" id="ShowVDpopupTitle">New Operator</h4>
      </div>
      <form action="" name="vd_edit_rate_form" class="form-horizontal" id="vd_edit_rate_form" method="post">
        <input type="hidden" name="vd_rate_edit_id" id="vd_rate_edit_id" />
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12 col-xs-12 m-t-20 m-b-20">
              <div class="form-group">
                <div class="col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="col-sm-4 line-height32" for="">Operator Name</label>
                    <div class="col-sm-6">
                      <input type="text" id="operator_name" onkeypress="alphanumeric_only(this.id)" class="form-control" value="" placeholder="Enter Name" name="operator_name" autocomplete="off"  />
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="col-sm-4 line-height32" for="">Operator ID</label>
                    <div class="col-sm-6">
                      <input type="text" id="operator_id" onkeypress="alphanumeric_only(this.id)" class="form-control" value="" placeholder="Enter ID" name="operator_id" autocomplete="off" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <h4 id="ShowVDpopupTitle" class="col-sm-12 col-xs-12">Operating Countries</h4>
            <div class="col-sm-12 col-xs-12 m-t-20 m-b-20">
              <div class="form-group">
                <div class="col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="col-sm-12 text-center"> <strong class=" line-height32" for="">Remaining Countries</strong> </div>
                    <div class="col-sm-12 text-center m-t-20 err-align">
                      <select id="from_select_list" class="form-control" multiple="multiple" name="from_select_list[]">
                      </select>
                    </div>
                  </div>
                </div>
                
                <!-- Desktop view -->
                
                <div class="col-sm-2 text-center swap-btn m-t-20 hidden-xs">
                  <div class="col-sm-12 col-xs-12 text-center swap-btn m-t-20"> &nbsp; </div>
                  <div class="col-sm-12 col-xs-3 text-center">
                    <button id="moveright" type="button" class="btn btn-primary" onclick="move_list_items('from_select_list','to_select_list');"> <i class="fa fa-angle-right"></i> </button>
                  </div>
                  <div class="col-sm-12 col-xs-3 text-center">
                    <button id="moverightall" type="button" class="btn btn-primary" onclick="move_list_items_all('from_select_list','to_select_list');" > <i class="fa fa-angle-double-right"></i> </button>
                  </div>
                  <div class="col-sm-12 col-xs-3 text-center">
                    <button id="moveleft" type="button" class="btn btn-primary" onclick="move_list_items('to_select_list','from_select_list');" > <i class="fa fa-angle-left"></i> </button>
                  </div>
                  <div class="col-sm-12 col-xs-3 text-center">
                    <button id="moveleftall" type="button" class="btn btn-primary" onclick="move_list_items_all('to_select_list','from_select_list');" > <i class="fa fa-angle-double-left"></i> </button>
                  </div>
                </div>
                
                <!-- Mobile view -->
                
                <div class="col-sm-2 col-xs-12 text-center swap-btn m-t-20 hidden-sm  hidden-md  hidden-lg ">
                  <div class="col-sm-12 col-xs-3 text-center">
                    <button id="moveright" type="button" class="btn btn-primary" onclick="move_list_items('from_select_list','to_select_list');"> <i class="fa fa-angle-down"></i> </button>
                  </div>
                  <div class="col-sm-12 col-xs-3 text-center">
                    <button id="moverightall" type="button" class="btn btn-primary" onclick="move_list_items_all('from_select_list','to_select_list');" > <i class="fa fa-angle-double-down"></i> </button>
                  </div>
                  <div class="col-sm-12 col-xs-3 text-center">
                    <button id="moveleft" type="button" class="btn btn-primary" onclick="move_list_items('to_select_list','from_select_list');" > <i class="fa fa-angle-up"></i> </button>
                  </div>
                  <div class="col-sm-12 col-xs-3 text-center">
                    <button id="moveleftall" type="button" class="btn btn-primary" onclick="move_list_items_all('to_select_list','from_select_list');" > <i class="fa fa-angle-double-up"></i> </button>
                  </div>
                </div>
                <div class="col-sm-5 col-xs-12">
                  <div class="row">
                    <div class="col-sm-12 text-center Mm-t-20"> <strong class=" line-height32" for="">Operating Countries</strong> </div>
                    <div class="col-sm-12 text-center m-t-20">
                      <select id="to_select_list" multiple="multiple" name="to_select_list[]" class="form-control">
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success vd_rate_submit" id="vd_rate_submit" name="vd_rate_submit" data-dismiss="modal" > Save </button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancel </button>
        </div>
      </form>
    </div>
  </div>
  <!-- /.modal-content --> 
</div>
<!-- /.modal-dialog -->

</div>
<!-- END MAIN CONTENT -->