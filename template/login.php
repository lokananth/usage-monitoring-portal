<?php include('header.php');?>
<style type="text/css">
.fixed-header #main{margin-left:0px;}
.smart-style-4 #content>.row:first-child{background:none !important; border:none !important;}
</style>	  
		<!-- MAIN PANEL -->
		<div id="main" role="main">
	<!-- MAIN CONTENT -->
			<div id="content">

				<!-- row -->
				<div class="row"> 
					<div class="col-sm-offset-3 col-sm-6 col-xs-12">
					 <div id="content" class=" full-page login">
				  
							<form action="index.html" id="login-form" class="smart-form client-form">
							 <img src="img/logo-mundio1.png" alt class="logo"> 
								<header>
									<b>Device Management System</b>
								</header>
								<fieldset> 
									<section>
										<label class="label">E-mail</label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input type="email" name="email">
											<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address/username</b></label>
									</section>
									<section>
										<label class="label">Password</label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" name="password">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
										<!-- <div class="note">
											<a href="forgotpassword.html">Forgot password?</a>
										</div> -->
									</section> 
								</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary">
										Sign in
									</button>
								</footer>
							</form>

						</div>
							 
				
						</div>
				
					</div>

				<!-- end row -->

			</div>
			
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

<?php include('footer.php');?>